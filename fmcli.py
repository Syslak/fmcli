#!/usr/bin/env python3

from dataclasses import dataclass
from enum import Enum
import sys
import os
from typing import Any
import requests
import pandas as pd
from languages.languages import add_languages_to_dataframe, songs_by_language

try:
    from key import api_key, default_user
except ModuleNotFoundError:
    print("Err")
    os._exit(1)


class Operation(Enum):
    TOP_TRACKS = lambda args: fetch_data(args, {
        "method": "user.gettoptracks",
        "datarows": lambda x: x["toptracks"]["track"],
        "fields": {
            "Song": lambda x: x['name'],
            "Artist": lambda x: x["artist"]["name"],
            "Count": lambda x: x["playcount"]
        }
    })
    TOP_BANDS = lambda args: fetch_data(args, {
        "method": "user.gettopartists",
        "datarows": lambda x: x["topartists"]["artist"],
        "fields": {
            "Artist": lambda x: x["name"],
            "Count": lambda x: x["playcount"]
        }
    })
    TOP_ALBUMS = lambda args: fetch_data(args, {
        "method": "user.gettopalbums",
        "datarows": lambda x: x["topalbums"]["album"],
        "fields": {
            "Album": lambda x: x["name"],
            "Artist": lambda x: x["artist"]["name"],
            "Count": lambda x: x["playcount"]
        }
    })
    RECENT_REPORT = lambda args: fetch_data(args, {
        "method": "user.getweeklytrackchart",
        "datarows": lambda x: x["toptracks"]["track"],
        "fields": {
            "Song": lambda x: x['name'],
            "Artist": lambda x: x["artist"]["name"],
            "Count": lambda x: x["playcount"]
        }
    })
    TOP_LANGUAGES = lambda args: songs_by_language(
        fetch_data(args, {
            "method": "user.gettoptracks",
            "datarows": lambda x: x["toptracks"]["track"],
            "fields": {
                "Song": lambda x: x['name'],
                "Artist": lambda x: x["artist"]["name"],
                "Count": lambda x: x["playcount"]
            }
        })
    )

@dataclass
class Arguments:
    api: str = api_key
    user: str = default_user
    period: str = "1month"
    print_lang: bool = False
    get: int = Operation.TOP_TRACKS
    length: int = 25


EX_USAGE = 64
def main():
    print()

    args = sys.argv[1:]
    arguments = parse_args(args)
    chart = arguments.get(arguments)

    if chart is not None and arguments.print_lang:
        chart = add_languages_to_dataframe(chart)

    if chart is not None:
        print_chart(chart)

def parse_args(args) -> Arguments:
    man = """FMCLI - View listening reports from the commandline
    fmcli will print user top tracks last month by default.
    Usage:
        fmcli [flags]
    Example:
        fmcli -l 10 --user user
        will will return top 10 tracks for the user named user from last month.
    Optional arguments:
        -h, --help              Print help page
        -u, --user              Specify user
        -l, --length            Specify length of tracklist
        -w, --week, --weekly    Print weekly top chart
        -m, --month, --montly   Print montly top chart
        -p, --period            Specify period, valid periods are: 7day, 1month, 3month, 6month, 12month
        -b, --band              Print top bands (artists)
        -a, --album             Print top albums
        --lang                  Prints a chart of top languages
        --print-lang            Adds a column showing the language
    """

    arguments = Arguments()
    valid_periods = ["7day", "1month", "3month", "6month", "12month", "7", "1", "3", "6", "12", "all"]
    while args:
        arg = args.pop(0)
        if arg == "-h" or arg == "--help":
            print(man)
            sys.exit(0)
        elif arg == "-u" or arg == "--user":
            try:
                arguments.user = args.pop(0)
            except IndexError:
                print("Not enough arguments provided for --user flag. <fmcli --user \"user\">")
                os._exit(EX_USAGE)

        elif arg == "-l" or arg == "--length":
            try:
                tmp = args.pop(0)
            except IndexError:
                print("Not enough arguments proveded for --length flag. <fmcli --length \"number\">")
                os._exit(EX_USAGE)
            try:
                arguments.length = int(tmp)
            except ValueError:
                print("Specified length must be an integer!")
                os._exit(EX_USAGE)
            if arguments.length > 1000:
                print("Specified length must be less than 1000")
                os._exit(EX_USAGE)
        elif arg == "-p" or arg == "--period":
            try:
                arguments.period = args.pop(0)
            except IndexError:
                print(f"Not enough arguments provided for --period flag. <fmcli --period \"period\"\nValid periods are {valid_periods}")
                os._exit(EX_USAGE)
            if arguments.period not in valid_periods:
                print(f"Valid period not gived, here is a list of valid periods {valid_periods}")
                os._exit(EX_USAGE)
            arguments.period = get_period(arguments.period)

        elif arg == "-w" or arg == "--week" or arg == "--weekly":
            arguments.period = "7day"
        elif arg == "-m" or arg == "--month" or arg == "--monthly":
            arguments.period == "1month"
        elif arg == "-b" or arg == "--band":
            arguments.get = Operation.TOP_BANDS
        elif arg == "-a" or arg == "--album":
            arguments.get = Operation.TOP_ALBUMS
        elif arg == "-a" or arg == "--lang":
            arguments.get = Operation.TOP_LANGUAGES
        elif arg == "--print-lang":
            arguments.print_lang = True
        else:
            print(f"Unknown arg {arg}\n use --help for more info.")
            os._exit(EX_USAGE)

    return arguments

def get_period(period):
    if period == "1":
        return "1month"
    elif period == "7":
        return "7day"
    elif period == "3":
        return "3month"
    elif period == "6":
        return "6month"
    elif period == "12":
        return "12month"
    elif period == "all":
        return "overall"
    else:
        return period


def print_chart(data: pd.DataFrame) -> None:
    """Prints a Pandas DataFrame in the classic FMCLI
    chart style.

    Args:
        data (pd.DataFrame): The DataFrame to print
    """
    # Get max length for each column
    lengths = { col: max(data[col].str.len()) for col in data.columns }
    
    # Generates a header and prints it
    header = "  ".join(
        ["Rank", *[ f"{col}{' '*(lengths[col]-len(col))}" for col in data.columns ]]
    )
    print(f"\033[0;1;4m{header} \033[0m")

    # Print each row
    for i, row in data.iterrows():
        # Color every other line
        if i % 2 == 0:
            print("\033[48;5;237m", end="")
        
        # Print the rank
        print(f" {i+1}{' '*(5-len(str(i+1)))}", end="")

        # Print the values
        print('  '.join(
            [f"{row[col]}{' '*(max(lengths[col], len(col))-len(row[col]))}" for col in data.columns ]
        ), end="")

        # Clear formatting and print a new line
        print(" \033[0m")


def fetch_data(args: Arguments, op: dict[str, Any]) -> pd.DataFrame:
    url = f"https://ws.audioscrobbler.com/2.0/?method={op['method']}&user={args.user}&api_key={args.api}\
&period={args.period}&format=json&limit={args.length}"
    req = requests.get(url).json()

    values = { key: [] for key in op["fields"].keys() }

    if "error" in req.keys():
        print(f"Error! last fm api responed with:\n{req}")
        os._exit(1)

    for data in op['datarows'](req):
        for field, getter in op["fields"].items():
            values[field].append(getter(data))
    return pd.DataFrame({
        key: pd.Series(value) for key, value in values.items()
    })


if __name__ == "__main__":
    main()
