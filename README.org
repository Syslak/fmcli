* FMCLI

View listening reports from the commandline

** Dependencies
#+BEGIN_SRC
pip3 install requests pandas
#+END_SRC
** Installation

#+BEGIN_SRC
git clone https://gitlab.com/Syslak/fmcli.git
#+END_SRC

#+BEGIN_SRC
cd fmcli
sudo make install
#+END_SRC



** Usage
fmcli will print user top tracks last month by default.
#+BEGIN_SRC
    fmcli [flags]
#+END_SRC

*** Example:
#+BEGIN_SRC
    fmcli -l 10 --user user
#+END_SRC
    will will return top 10 tracks for the user named user from last month.
*** Optional arguments:

#+BEGIN_SRC
    -h, --help              Print help page
    -u, --user              Specify user
    -l, --length            Specify length of tracklist
    -w, --week, --weekly    Print weekly top chart
    -m, --month, --montly   Print montly top chart
    -p, --period            Specify period, valid periods are: 7day, 1month, 3month, 6month, 12month (7, 1, 3, 6, 12)
    -b, --band              Print top bands (artists)
    -a, --album             Print top albums
    --lang                  Prints a chart of top languages
    --print-lang            Adds a column showing the language
#+END_SRC
